#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>

void daemonize(void) {

      pid_t pid = fork();

      if(pid < 0) {
            exit(EXIT_FAILURE);
      }else if(pid > 0) {

            /*
             * Terminate parent. It's the child that's interesting. When the 
             * parent dies, the child process will become a child of init.
             */
            exit(EXIT_SUCCESS);
            
      }else if(pid == 0) {

            /* If pid == 0, than we are the child process. The parent will 
             * concurently execute but not this block and will proceed below 
             * this block and there we terminate the parent. It's not needed.
             */


            /*
             * Un-mask any file mode that was inherited, giving It all access 
             * to create/modify any file. This is how daemons normally operates.
             */
            umask(0);
            
            /* 
             * A daemon should operate independebly from other procesess, thus
             * should not belong to a group of other processes.
             * A daemon should not be getting signals from a controling terminal.
             *
             * When a user logs out from a session, all processes from that 
             * session are terminated. 
             *
             * Detach child from parent process session/groups. and controling 
             * terminal.  
             *
             * Note that by calling setsid() we become a session leader and a 
             * leader can aquire a controling terminal. To really make sure we 
             * can't get control we could fork a second time and let the parent
             * keep on living because than the parent would be the session 
             * leader and only a leader can aquire an controlling terminal. 
             * The child could call setsid to aquire an controling terminal but
             * why would you do that as a programmer if you are intent on 
             * daemonize?
             *
             * However, in this example we don't bother.
             *
             * Calling setsid() will create a new session where the child will 
             * become the session leader as well as group leader.
             */
            pid_t pid = setsid();
            if(pid == -1) {
                  perror("setsid() failed.");
                  exit(EXIT_FAILURE);
            }
            
            /*
             * Change Working DIR.
             */
            if(chdir("/") < 0 ) {
                  exit(EXIT_FAILURE);
            }
            /*
             * Close inherited descriptors and standard IO. Here, we choose to
             * pipe std.io to /dev/null because it is much safer. This is because
             * we might invoke libraries or external programs and these in turn
             * might need to use std.io and could fail if these are not opened. 
             */
            freopen("/dev/null", "r", stdin);
            freopen("/dev/null", "w", stdout);
            freopen("/dev/null", "w", stderr);

            /*
             * The child inherits the parent's open file descriptors. 
             * Close all but STD IO (we piped these in the above code). 
             */
            int maxfd = sysconf(_SC_OPEN_MAX);
            for(int fd=3; fd<maxfd; fd++)
                  close(fd);
                        
      }      
}


int main(int argc, char *argv[]) {

      daemonize();

      //do the work that we need to do, whatever that might be...
      //while(1) {
      //    ...
      //}
      
      return 0;
}
