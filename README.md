# Introduction
The intention of this project is to explain, for someone new to the topic, how to daemonize a program,
using the c programming language. I made this guide for myself but maybe It could be usefull for
someone else.

Below are some resources:

[Advanced programming in the UNIX Environment](http://www.apuebook.com/), by Rich Stevens.<br>
[Microhowto - to the point](http://www.microhowto.info/howto/cause_a_process_to_become_a_daemon.html)
<br><br>

A daemon is a program, a process that runs in the background, and is a child of
init and is not attached to a controling terminal. 

The work of daemonizing a program in C, is mostly done by two system
calls, fork() and setsid(). 

# The fork() call
```c
pid_t pid = fork();
```
fork() is a system call that creates a child process. It will do that by copying
the adress space of the current running process (parent) into a new adress space
(child). Since they are identical,
It means the instruction pointer will be pointing to the same instruction to
be executed, for both parent and child. To make these processes (programs) do
different tasks, we branch based on the return value of fork(). The fork() call,
being a **systemcall**, will actually return different values to the different
adress spaces (processes), depending on that adress space being a parent or
child. 

The fork() call will return one of three
values:

 * -1   means fork failed
 * 0    indicates that we are now executing as the child process
 * pid  an actual pid number (of the child) indicating that we are now executing as the parent

# The setsid() call
When a user logs out from a session, all procesess associated from that session is
terminated. Also, we don't want to belong to a controling terminal, because that
means we can receive kill signals from that terminal. These are not circumstances
that we wish for a daemon process. Therefore our process will detach itself from
these wordly constraints.

The setsid() call will create a new session with a new controlling group. The
calling process will become the session leader and the process group leader.
In other words, we now a session and controlling group of which there is only
one process member; the process that made the call.

The setsid() call can return -1 if, for whatever reason, it fails. It's good practice
to check for that. If setsid() succeeds, It will return the new group ID of the
calling process. 

# The little things...
Besides setsid() and fork() we clear the umask, because we don't want our child
to be limited by whatever file permissions the parent had. We also change the
working directory to / , because that's normal for a daemon. 
We close any open file descriptors that was inherited, except for STD IO, those
we pipe to /dev/null, because the daemon might utilize libraries that need to 
write to STDIO. 
